[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

export PATH=/opt/local/bin:/opt/local/sbin:/Users/w1zeman1p/bin:$PATH
export PATH="$HOME/Library/Haskell/bin:$PATH"
export PATH="$PATH:/Applications/Postgres.app/Contents/Versions/9.3/bin"
export PATH="$PATH:/usr/local/Cellar/android-sdk/23.0.2/tools"
export PATH="$PATH:/usr/local/Cellar/android-sdk/23.0.2/platform-tools"
export PATH=/Users/w1zeman1p/.rvm/gems/ruby-1.9.3-p392/bin:$PATH
export PATH="/usr/local/git/bin:$PATH"
export NODE_PATH=/usr/local/lib/node_modules:$NODE_PATH
#export PATH=/Applications/Postgres.app/Contents/MacOS/bin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.

[[ -s /Users/w1zeman1p/.nvm/nvm.sh ]] && . /Users/w1zeman1p/.nvm/nvm.sh # This loads NVM
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
bind -r '\C-s'
stty -ixon
