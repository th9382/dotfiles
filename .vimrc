set nocompatible     " be iMproved
filetype off         " required!
set rtp+=~/.vim/bundle/vundle/
set shell=bash\ -i
call vundle#rc()

Bundle 'gmarik/vundle'
Bundle 'maksimr/vim-jsbeautify'
Bundle 'scrooloose/nerdtree'
Bundle 'kien/ctrlp.vim'
Bundle 'MarcWeber/vim-addon-mw-utils'
Bundle 'tomtom/tlib_vim'
Bundle 'w1zeman1p/vim-snippets'
Bundle 'w1zeman1p/vim-snipmate'
Bundle 'tpope/vim-rails'
Bundle 'tpope/vim-unimpaired'
Bundle 'vim-ruby/vim-ruby'
Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-bundler'
Bundle 'tpope/gem-ctags'
Bundle 'scrooloose/syntastic'
Bundle 'christoomey/vim-tmux-navigator'
Bundle 'vim-scripts/tComment'
Bundle 't9md/vim-ruby-xmpfilter'
Bundle 'jnwhiteh/vim-golang'
Bundle 'vim-scripts/dbext.vim'
Bundle 'thoughtbot/vim-rspec'
Bundle 'derekwyatt/vim-scala'

filetype plugin indent on
syntax on

let mapleader = ","
let g:mapleader = ","
let NERDTreeShowHidden=1                         "hide nerd tree by default
let g:tmux_navigator_no_mappings = 1
let g:syntastic_javascript_checkers = ['jslint'] "synatx hilight js errors
let g:syntastic_javascript_jshint_conf="~/.jslintrc"

nnoremap <silent> {Left-mapping} :TmuxNavigateLeft<cr>
nnoremap <silent> {Down-Mapping} :TmuxNavigateDown<cr>
nnoremap <silent> {Up-Mapping} :TmuxNavigateUp<cr>
nnoremap <silent> {Right-Mapping} :TmuxNavigateRight<cr>
nnoremap <silent> {Previoust-Mapping} :TmuxNavigatePrevious<cr>

set background=dark
colorscheme molokai
set tabstop=2
set softtabstop=2
set shiftwidth=2                    "set default indenting to 2 spaces
set expandtab
set noimdisable
set iminsert=0
set imsearch=0
set noswapfile
set relativenumber                  "set relative numberlines
set number                          "show the line number of the one i'm on
set textwidth=72
set colorcolumn=81                  "show the 80 char rule
set laststatus=2
command WQ wq                       "meant wq
command Wq wq                       "meant wq
command W w                         "meant w
command Q q                         "meant q
command Bu BundleInstall            "shortcut for installing Bundles
map <c-f> :call JsBeautify()<cr>

" run ruby code in vim buffer
let g:xmpfilter_cmd = "seeing_is_believing"
autocmd FileType ruby nmap <buffer> ,m <Plug>(seeing_is_believing-mark)
autocmd FileType ruby xmap <buffer> ,m <Plug>(seeing_is_believing-mark)
autocmd FileType ruby imap <buffer> ,m <Plug>(seeing_is_believing-mark)
autocmd FileType ruby nmap <buffer> ,c <Plug>(seeing_is_believing-clean)
autocmd FileType ruby xmap <buffer> ,c <Plug>(seeing_is_believing-clean)
autocmd FileType ruby imap <buffer> ,c <Plug>(seeing_is_believing-clean)
" xmpfilter compatible
autocmd FileType ruby nmap <buffer> ,r <Plug>(seeing_is_believing-run_-x)
autocmd FileType ruby xmap <buffer> ,r <Plug>(seeing_is_believing-run_-x)
autocmd FileType ruby imap <buffer> ,r <Plug>(seeing_is_believing-run_-x)
" auto insert mark at appropriate spot.
autocmd FileType ruby nmap <buffer> ,e <Plug>(seeing_is_believing-run)
autocmd FileType ruby xmap <buffer> ,e <Plug>(seeing_is_believing-run)
autocmd FileType ruby imap <buffer> ,e <Plug>(seeing_is_believing-run)

autocmd FileType css noremap <buffer> <c-f> :call CSSBeautify()<cr>
autocmd BufNewFile,BufRead *.html.erb set filetype=html
autocmd BufNewFile,BufRead *.jst.ejs set filetype=html
autocmd BufNewFile,BufRead *.jst set filetype=html
autocmd BufNewFile,BufRead *.hbs set filetype=html
autocmd BufNewFile,BufRead *.rabl set filetype=rb
autocmd BufNewFile,BufRead *.jbuilder set filetype=rb
autocmd BufNewFile,BufRead *Gemfile set filetype=rb

" show spelling mistakes as underlines
autocmd BufRead,BufNewFile *.md setlocal spell
highlight clear SpellBad
highlight SpellBad cterm=underline

" hilight extra white space at ends of lines
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
autocmd BufWritePre *.rb :call TrimWhiteSpace()
autocmd BufWritePre *.scss :call TrimWhiteSpace()
autocmd BufWritePre *.css :call TrimWhiteSpace()
autocmd BufWritePre *.js :call TrimWhiteSpace()

" use ctrl-s to save
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a
imap <c-s> <Esc><c-s>

" stop using arrow keys
imap <Up> WATTTT!
imap <Down> WATTTT!
imap <Left> WATTTT!
imap <Right> WATTT!

" copy visualy selected text to clip board
vmap <C-x> :!pbcopy<CR>
vmap <C-c> :w !pbcopy<CR><CR>

" copy from buffer into temp file to paste in another vim buffer
vmap <silent> ,y y:new<CR>:call setline(1,getregtype())<CR>o<Esc>P:wq! ~/reg.txt<CR>
nmap <silent> ,y :new<CR>:call setline(1,getregtype())<CR>o<Esc>P:wq! ~/reg.txt<CR>
map <silent> ,p :sview ~/reg.txt<CR>"zdddG:q!<CR>:call setreg('"', @", @z)<CR>p
map <silent> ,P :sview ~/reg.txt<CR>"zdddG:q!<CR>:call setreg('"', @", @z)<CR>P

" rspec vim
map <Leader>t :call RunCurrentSpecFile()<CR>
map <Leader>s :call RunNearestSpec()<CR>
map <Leader>l :call RunLastSpec()<CR>
map <Leader>a :call RunAllSpecs()<CR>

if has("autocmd")
  filetype indent on
endif
function! TrimWhiteSpace()
  %s/\s\+$//e
endfunction
augroup markdown
  au!
  au BufNewFile,BufRead *.md,*.markdown setlocal filetype=ghmarkdown
augroup END
